import React, { Component, Fragment } from 'react';
import Modal from './components/UI/Modal/Modal';
import Alert from './components/UI/Alert/Alert'
import './App.css';

class App extends Component {
  state = {
      modalShow: false,
      showAlert1: true,
      showAlert2: true
  };


  modalShow = () => {
    this.setState({modalShow: true})
  };

  modalClose = () => {
    this.setState({modalShow: false})
  };

  showAlert = () => {
      this.setState({
          showAlert: true
      })
  };
    someHandle = () => {
      this.setState({showAlert1: false})
    };


  render() {
    return (
        <Fragment>
        <Modal show={this.state.modalShow}
               closed={this.modalClose}
               title="Some kinda modal title"
        >
            <p>This modal content</p>
        </Modal>

            <button onClick={this.modalShow}>show modal</button>


            <Alert
                show={this.state.showAlert1}
                type="success">
                This is a warning type alert
                <button className="dismis"  onClick={this.someHandle}>x</button>
            </Alert>
            <Alert type="danger" show={this.state.showAlert2}>
                This is a warning type alert
            </Alert>
        </Fragment>

    );
  }
}

export default App;
