import React, {Fragment} from 'react';

import './Modal.css';
import Backdrop from "../Backdrop/Backdrop";

const Modal = props => {
    return (
        <Fragment>
            <Backdrop show={props.show} onClick={props.close}/>
        <div className="Modal" style={{display: props.show ? 'block' : 'none'}}>
            <div className="modal-block clearfix">
                <h3 className="modal-title">{props.title}</h3>
                <button className="btn-close" onClick={props.closed}><span>x </span></button>
            </div>
            <div className="modal-content">
                {props.children}
            </div>

        </div>
        </Fragment>
    );
};

export default Modal;