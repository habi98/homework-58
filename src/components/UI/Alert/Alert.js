import React from 'react';

import './Alert.css'

const Alert = (props) => {
    return (
        props.show ?   <div className= {`Alert  ${props.type} clearfix`}>
            {props.children}
    </div>: true

    );
};

export default Alert;